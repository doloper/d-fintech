import React from "React"
import {Component} from "React"
import {Link} from "react-router-dom";

const API = "http://5.201.185.69:8123/tickets";

export class Tickets extends Component{
    constructor(props){
        super(props)
        this.state = {
            status: 1,
            data: null,
        }

        this.getData()
    }

    getData() {
        fetch(API)
        .then(response => response.json())
            .then((jsonData) => {
                this.state.status = 0;
                this.state.data = jsonData;

                this.forceUpdate();
            })
            .catch((error) => {
                this.state.status = 2;
                // handle your errors here
                console.error(error)
            })

    }

    render(){
        if(this.state.status == 1){
            return(
                <section id={"Dashboard"}>
                    <div className={"container"} >
                        <h1>Tickets List</h1>
                        <p>Loading ...</p>
                    </div>
                </section>
            )
        } else if(this.state.status == 0) {
            const items = []
            for (const [index, value] of this.state.data.entries()) {
                items.push(
                    <tr>
                        <td>{value.ticketID}</td>
                        <td>
                            <Link to={`/ticket/${value.ticketID}`}>
                                {value.ticketNumber}
                            </Link>
                        </td>
                        <td>{value.startDate}</td>
                        <td>{value.expireDate}</td>
                        <td>{value.hours}</td>
                    </tr>
                )
            }
            return(
                <section id={"Dashboard"}>
                    <div className={"container"} >
                        <h1 className={"mb-3"}>Tickets List</h1>
                        <table className="table">
                            <thead className="thead-light">
                            <tr>
                                <th scope="col">Ticket ID</th>
                                <th scope="col">Ticket Number</th>
                                <th scope="col">Start Date</th>
                                <th scope="col">Expire Date</th>
                                <th scope="col">Hours</th>
                            </tr>
                            </thead>
                            <tbody>
                            {items}
                            </tbody>
                        </table>
                    </div>
                </section>
            )
        } else {
            return(
                <section id={"Dashboard"}>
                    <div className={"container"} >
                        <h1>Tickets List</h1>
                        <p>Error on fetch data from server!</p>
                    </div>
                </section>
            )
        }

    }

}