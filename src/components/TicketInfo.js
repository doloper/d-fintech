import React from "react"
import {useState} from "react"
import {Component} from "react"

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';



const API = "http://5.201.185.69:8123/tickets"


export class TicketInfo extends Component{
    constructor(props){
        super(props)

        this.handleStartDayChange = this.handleStartDayChange.bind(this);

        this.state = {
            status: 1,
            data: null,
            startDate: undefined,
            expireDate: undefined,
            startHour: undefined,
            endHour: undefined,
        }

        this.getData()
    }


    getData() {
        fetch(API)
            .then(response => response.json())
            .then((jsonData) => {
                this.state.status = 3
                for(let i = 0; i < jsonData.length ; i++){
                    if(jsonData[i].ticketID == this.props.match.params.id){
                        this.state.status = 0
                        this.state.data = jsonData[i];
                        this.state.startDate = jsonData[i]["startDate"]
                        this.state.expireDate = jsonData[i]["expireDate"]

                        let hours = jsonData[i]["hours"].split("-")
                        this.state.startHour = hours[0]
                        this.state.endHour = hours[1]
                    }
                }

                this.forceUpdate();
            })
            .catch((error) => {
                this.state.status = 2;
                // handle your errors here
                console.error(error)
            })
    }


    handleStartDayChange(day) {
        let d = day.getDate()
        let m = day.getMonth()+ 1
        let y = day.getFullYear()

        this.state.startDate = m + "/" + d + "/" + y;
        console.log(m + "/" + d + "/" + y)
    }


    handleExpireDayChange(day) {
        let d = day.getDate()
        let m = day.getMonth()+ 1
        let y = day.getFullYear()

        this.state.expireDate = m + "/" + d + "/" + y;
        console.log(m + "/" + d + "/" + y)
    }


    render(){
        if(this.state.status == 1){
            return(
                <section id={"Dashboard"}>
                    <div className={"container"} >
                        <h1>Details of ticket {this.props.match.params.id}</h1>
                        <p>Loading ...</p>
                    </div>
                </section>
            )
        } else if(this.state.status == 0){
            const FORMAT = 'MM/dd/yyyy';
            return(
                <section id={"Dashboard"}>
                    <div className={"container"} >
                        <h1 className={"mb-3"}>Details of ticket {this.props.match.params.id}</h1>
                        <form>
                            <div className="form-group">
                                <label htmlFor="ticketid">Ticket ID</label>
                                <input type="text" className="form-control" id="ticketid" name={"ticketid"}
                                       value={this.state.data.ticketID} disabled />
                            </div>
                            <div className="form-group">
                                <label htmlFor="ticketnum">Ticket Number</label>
                                <input type="text" className="form-control" id="ticketnum" name={"ticketnum"}
                                       value={this.state.data.ticketNumber} disabled />
                            </div>


                            <div className={"row"} >
                                <div className="form-group col-lg-6">
                                    <label htmlFor="ticketid">Start Date</label>
                                    <br/>
                                    <DayPickerInput
                                        format={FORMAT}
                                        onDayChange={this.handleStartDayChange}
                                        value={new Date(this.state.data.startDate)}
                                    />
                                </div>

                                <div className="form-group col-lg-6">
                                    <label htmlFor="ticketid">End Date</label>
                                    <br/>
                                    <DayPickerInput
                                        format={FORMAT}
                                        onDayChange={this.handleExpireDayChange}
                                        value={new Date(this.state.data.expireDate)}
                                    />
                                </div>


                                <div className="form-group col-lg-6">
                                    <label htmlFor="starthour">Start Hour</label>
                                    <input type="text" className="form-control" id="starthour" name={"starthour"}
                                           value={this.state.startHour} />
                                </div>

                                <div className="form-group col-lg-6">
                                    <label htmlFor="starthour">End Hour</label>
                                    <input type="text" className="form-control" id="starthour" name={"starthour"}
                                           value={this.state.endHour} />
                                </div>

                            </div>


                            <button type="submit" className="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </section>
            )
        } else if(this.state.status == 3){
            return(
                <section id={"Dashboard"}>
                    <div className={"container"} >
                        <h1>Details of ticket {this.props.match.params.id}</h1>
                        <p>Ticket not found!</p>
                    </div>
                </section>
            )
        }
    }
}