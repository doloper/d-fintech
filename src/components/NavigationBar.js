import React from "react"
import {Component} from "react"
import {Link} from "react-router-dom";

export class NavigationBar extends Component{

    render(){
        return(
            <section id={"navigationBar"}>
                <div className={"container p-0"} >
                    <ul>
                        <Link to="/">
                            <li>Dashboard</li>
                        </Link>
                        <Link to="/users">
                            <li>Users</li>
                        </Link>
                        <Link to="/tickets">
                            <li>Tickets</li>
                        </Link>
                    </ul>
                </div>
            </section>
        )
    }

}