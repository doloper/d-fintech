import React from 'react'
import {render} from 'react-dom'
import {Index} from "./components/Index";
import {NavigationBar} from "./components/NavigationBar";
import {Error404} from "./components/Error404";
import {Users} from "./components/Users";
import {UserInfo} from "./components/UserInfo";
import {TicketInfo} from "./components/TicketInfo";
import {Tickets} from "./components/Tickets";
import 'bootstrap/dist/css/bootstrap.min.css';
import './stylesheets/style.scss';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";


window.React = React

// render(
//     <SkiDayCount total={"test"} /> ,
//     document.getElementById("react-container")
// )

render(
    <Router>
        <Route path="/" component={NavigationBar} />

        <Switch>
            <Route exact path="/" component={Index} />

            <Route path="/users" component={Users} />

            <Route path="/tickets" component={Tickets} />

            <Route path="/user/:id" component={UserInfo} />

            <Route path="/ticket/:id" component={TicketInfo} />

            <Route path="/*" component={Error404} />
        </Switch>
    </Router>,
    document.getElementById("react-container")
);
